﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorBq
{
    public static class RecursoSistema
    {
        private static int[] recursos;
        private static int[] recursosIniciales;

        public static int[] Recursos { get => recursos; set => recursos = value; }
        public static int[] RecursosIniciales { get => recursosIniciales; set => recursosIniciales = value; }

        static RecursoSistema()
        {
            
        }
        public static void crearArreglo(int cantidad)
        {
             recursos = new int[cantidad];
            recursosIniciales = new int[cantidad];
                        for (int i = 0; i < cantidad; i++)
                        {
                            recursos[i] = 0;
                            recursosIniciales[i] = 0;
                        }
        }
        public static void cargarRecurso(int pos, int cantidad)
        {
            recursos[pos] = cantidad;
            recursosIniciales[pos] = cantidad;
        }
    }
}
