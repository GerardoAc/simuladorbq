﻿namespace SimuladorBq
{
    partial class Recursos
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelCPU = new System.Windows.Forms.Label();
            this.labelHDD = new System.Windows.Forms.Label();
            this.labelRAM = new System.Windows.Forms.Label();
            this.labelSDD = new System.Windows.Forms.Label();
            this.txtCPU = new System.Windows.Forms.TextBox();
            this.txtRAM = new System.Windows.Forms.TextBox();
            this.txtHDD = new System.Windows.Forms.TextBox();
            this.txtSSD = new System.Windows.Forms.TextBox();
            this.labelCPUActual = new System.Windows.Forms.Label();
            this.labelRamActual = new System.Windows.Forms.Label();
            this.labelHDDActual = new System.Windows.Forms.Label();
            this.labelSDDActual = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider2 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider3 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider4 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).BeginInit();
            this.SuspendLayout();
            // 
            // labelCPU
            // 
            this.labelCPU.AutoSize = true;
            this.labelCPU.Location = new System.Drawing.Point(58, 27);
            this.labelCPU.Name = "labelCPU";
            this.labelCPU.Size = new System.Drawing.Size(29, 13);
            this.labelCPU.TabIndex = 0;
            this.labelCPU.Text = "CPU";
            // 
            // labelHDD
            // 
            this.labelHDD.AutoSize = true;
            this.labelHDD.Location = new System.Drawing.Point(286, 27);
            this.labelHDD.Name = "labelHDD";
            this.labelHDD.Size = new System.Drawing.Size(31, 13);
            this.labelHDD.TabIndex = 1;
            this.labelHDD.Text = "HDD";
            // 
            // labelRAM
            // 
            this.labelRAM.AutoSize = true;
            this.labelRAM.Location = new System.Drawing.Point(161, 27);
            this.labelRAM.Name = "labelRAM";
            this.labelRAM.Size = new System.Drawing.Size(31, 13);
            this.labelRAM.TabIndex = 2;
            this.labelRAM.Text = "RAM";
            // 
            // labelSDD
            // 
            this.labelSDD.AutoSize = true;
            this.labelSDD.Location = new System.Drawing.Point(412, 27);
            this.labelSDD.Name = "labelSDD";
            this.labelSDD.Size = new System.Drawing.Size(29, 13);
            this.labelSDD.TabIndex = 3;
            this.labelSDD.Text = "SSD";
            // 
            // txtCPU
            // 
            this.txtCPU.Location = new System.Drawing.Point(23, 59);
            this.txtCPU.Name = "txtCPU";
            this.txtCPU.Size = new System.Drawing.Size(100, 20);
            this.txtCPU.TabIndex = 4;
            this.txtCPU.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCPU_KeyPress);
          // 
            // txtRAM
            // 
            this.txtRAM.Location = new System.Drawing.Point(140, 59);
            this.txtRAM.Name = "txtRAM";
            this.txtRAM.Size = new System.Drawing.Size(100, 20);
            this.txtRAM.TabIndex = 5;
            this.txtRAM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRAM_KeyPress);
            // 
            // txtHDD
            // 
            this.txtHDD.Location = new System.Drawing.Point(265, 59);
            this.txtHDD.Name = "txtHDD";
            this.txtHDD.Size = new System.Drawing.Size(100, 20);
            this.txtHDD.TabIndex = 6;
            this.txtHDD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHDD_KeyPress);
            // 
            // txtSSD
            // 
            this.txtSSD.Location = new System.Drawing.Point(386, 59);
            this.txtSSD.Name = "txtSSD";
            this.txtSSD.Size = new System.Drawing.Size(100, 20);
            this.txtSSD.TabIndex = 7;
            this.txtSSD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSSD_KeyPress);
            // 
            // labelCPUActual
            // 
            this.labelCPUActual.AutoSize = true;
            this.labelCPUActual.Location = new System.Drawing.Point(38, 97);
            this.labelCPUActual.Name = "labelCPUActual";
            this.labelCPUActual.Size = new System.Drawing.Size(62, 13);
            this.labelCPUActual.TabIndex = 8;
            this.labelCPUActual.Text = "CPU Actual";
            // 
            // labelRamActual
            // 
            this.labelRamActual.AutoSize = true;
            this.labelRamActual.Location = new System.Drawing.Point(157, 97);
            this.labelRamActual.Name = "labelRamActual";
            this.labelRamActual.Size = new System.Drawing.Size(64, 13);
            this.labelRamActual.TabIndex = 9;
            this.labelRamActual.Text = "RAM Actual";
            // 
            // labelHDDActual
            // 
            this.labelHDDActual.AutoSize = true;
            this.labelHDDActual.Location = new System.Drawing.Point(286, 97);
            this.labelHDDActual.Name = "labelHDDActual";
            this.labelHDDActual.Size = new System.Drawing.Size(64, 13);
            this.labelHDDActual.TabIndex = 10;
            this.labelHDDActual.Text = "HDD Actual";
            // 
            // labelSDDActual
            // 
            this.labelSDDActual.AutoSize = true;
            this.labelSDDActual.Location = new System.Drawing.Point(401, 97);
            this.labelSDDActual.Name = "labelSDDActual";
            this.labelSDDActual.Size = new System.Drawing.Size(63, 13);
            this.labelSDDActual.TabIndex = 11;
            this.labelSDDActual.Text = "SDD Actual";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // errorProvider2
            // 
            this.errorProvider2.ContainerControl = this;
            // 
            // errorProvider3
            // 
            this.errorProvider3.ContainerControl = this;
            // 
            // errorProvider4
            // 
            this.errorProvider4.ContainerControl = this;
            // 
            // Recursos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.Controls.Add(this.labelSDDActual);
            this.Controls.Add(this.labelHDDActual);
            this.Controls.Add(this.labelRamActual);
            this.Controls.Add(this.labelCPUActual);
            this.Controls.Add(this.txtSSD);
            this.Controls.Add(this.txtHDD);
            this.Controls.Add(this.txtRAM);
            this.Controls.Add(this.txtCPU);
            this.Controls.Add(this.labelSDD);
            this.Controls.Add(this.labelRAM);
            this.Controls.Add(this.labelHDD);
            this.Controls.Add(this.labelCPU);
            this.Name = "Recursos";
            this.Size = new System.Drawing.Size(517, 164);
            this.Load += new System.EventHandler(this.Recursos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCPU;
        private System.Windows.Forms.Label labelHDD;
        private System.Windows.Forms.Label labelRAM;
        private System.Windows.Forms.Label labelSDD;
        private System.Windows.Forms.TextBox txtCPU;
        private System.Windows.Forms.TextBox txtRAM;
        private System.Windows.Forms.TextBox txtHDD;
        private System.Windows.Forms.TextBox txtSSD;
        private System.Windows.Forms.Label labelCPUActual;
        private System.Windows.Forms.Label labelRamActual;
        private System.Windows.Forms.Label labelHDDActual;
        private System.Windows.Forms.Label labelSDDActual;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ErrorProvider errorProvider2;
        private System.Windows.Forms.ErrorProvider errorProvider3;
        private System.Windows.Forms.ErrorProvider errorProvider4;
    }
}
