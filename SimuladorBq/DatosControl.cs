﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorBq
{
    public partial class DatosControl : UserControl
    {
        public DatosControl()
        {
            InitializeComponent();
            toolTip1.SetToolTip(btnAzul, "Estado Seguro");
            toolTip1.SetToolTip(btnRojo, "Proceso Finalizado");
            toolTip1.SetToolTip(btnVerde, "Estado Inseguro");
            toolTip1.SetToolTip(btnVioleta, "El proceso no muere");
        }
        public void AgregarDatos(string nombre,string recursos,string timIni,string tiempPR,string recursosA,string tiempoV) {
            labNombre.Text ="Proceso:"+ nombre;
            labelRecursos.Text = "Recursos:" + recursos;
            labelTiempoIni.Text = "Tiempo Inanicion:" + timIni;
            labelTiempoPedirRecurso.Text = "TPR:"+tiempPR;
            labelRecursosAsi.Text ="Recursos Asignados:"+ recursosA;
            labelTiempoVida.Text = "Tiempo de Vida:" + tiempoV;
            toolTip1.SetToolTip(labelTiempoPedirRecurso, "este es el tiempo que, transcurrido," +
                " vuelve a solicitar o liberar recursos. Este tiempo solo avanza cuando" +
                " se este ejecutando, si esta pausado este tiempo no aumenta");
            toolTip1.SetToolTip(labelTiempoIni, "Cuanto tiempo le permite al proceso estar inactivo, antes de que sea eliminado");
            toolTip1.SetToolTip(labelRecursos,"Cuantos recursos necesita de cada uno para ejecutarse");
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
