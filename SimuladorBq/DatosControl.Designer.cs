﻿namespace SimuladorBq
{
    partial class DatosControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labNombre = new System.Windows.Forms.Label();
            this.labelRecursos = new System.Windows.Forms.Label();
            this.labelTiempoIni = new System.Windows.Forms.Label();
            this.labelTiempoPedirRecurso = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.labelRecursosAsi = new System.Windows.Forms.Label();
            this.labelTiempoVida = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnVioleta = new System.Windows.Forms.Button();
            this.btnVerde = new System.Windows.Forms.Button();
            this.btnRojo = new System.Windows.Forms.Button();
            this.btnAzul = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labNombre
            // 
            this.labNombre.AutoSize = true;
            this.labNombre.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labNombre.Location = new System.Drawing.Point(205, 0);
            this.labNombre.Name = "labNombre";
            this.labNombre.Size = new System.Drawing.Size(44, 13);
            this.labNombre.TabIndex = 1;
            this.labNombre.Text = "Nombre";
            // 
            // labelRecursos
            // 
            this.labelRecursos.AutoSize = true;
            this.labelRecursos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelRecursos.Location = new System.Drawing.Point(43, 28);
            this.labelRecursos.Name = "labelRecursos";
            this.labelRecursos.Size = new System.Drawing.Size(52, 13);
            this.labelRecursos.TabIndex = 2;
            this.labelRecursos.Text = "Recursos";
            // 
            // labelTiempoIni
            // 
            this.labelTiempoIni.AutoSize = true;
            this.labelTiempoIni.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelTiempoIni.Location = new System.Drawing.Point(193, 28);
            this.labelTiempoIni.Name = "labelTiempoIni";
            this.labelTiempoIni.Size = new System.Drawing.Size(56, 13);
            this.labelTiempoIni.TabIndex = 3;
            this.labelTiempoIni.Text = "Tiempo Ini";
            // 
            // labelTiempoPedirRecurso
            // 
            this.labelTiempoPedirRecurso.AutoSize = true;
            this.labelTiempoPedirRecurso.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelTiempoPedirRecurso.Location = new System.Drawing.Point(319, 28);
            this.labelTiempoPedirRecurso.Name = "labelTiempoPedirRecurso";
            this.labelTiempoPedirRecurso.Size = new System.Drawing.Size(112, 13);
            this.labelTiempoPedirRecurso.TabIndex = 4;
            this.labelTiempoPedirRecurso.Text = "Tiempo Pedir Recurso";
            // 
            // labelRecursosAsi
            // 
            this.labelRecursosAsi.AutoSize = true;
            this.labelRecursosAsi.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelRecursosAsi.Location = new System.Drawing.Point(46, 64);
            this.labelRecursosAsi.Name = "labelRecursosAsi";
            this.labelRecursosAsi.Size = new System.Drawing.Size(140, 13);
            this.labelRecursosAsi.TabIndex = 5;
            this.labelRecursosAsi.Text = "Recursos Asignados:0-0-0-0";
            this.labelRecursosAsi.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelTiempoVida
            // 
            this.labelTiempoVida.AutoSize = true;
            this.labelTiempoVida.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelTiempoVida.Location = new System.Drawing.Point(319, 64);
            this.labelTiempoVida.Name = "labelTiempoVida";
            this.labelTiempoVida.Size = new System.Drawing.Size(83, 13);
            this.labelTiempoVida.TabIndex = 6;
            this.labelTiempoVida.Text = "Tiempo de vida:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Lavender;
            this.panel1.Controls.Add(this.btnVioleta);
            this.panel1.Controls.Add(this.btnVerde);
            this.panel1.Controls.Add(this.btnRojo);
            this.panel1.Controls.Add(this.btnAzul);
            this.panel1.Location = new System.Drawing.Point(447, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(67, 88);
            this.panel1.TabIndex = 7;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnVioleta
            // 
            this.btnVioleta.BackColor = System.Drawing.Color.Violet;
            this.btnVioleta.Location = new System.Drawing.Point(36, 32);
            this.btnVioleta.Name = "btnVioleta";
            this.btnVioleta.Size = new System.Drawing.Size(27, 23);
            this.btnVioleta.TabIndex = 3;
            this.btnVioleta.UseVisualStyleBackColor = false;
            // 
            // btnVerde
            // 
            this.btnVerde.BackColor = System.Drawing.Color.Green;
            this.btnVerde.Location = new System.Drawing.Point(3, 32);
            this.btnVerde.Name = "btnVerde";
            this.btnVerde.Size = new System.Drawing.Size(27, 23);
            this.btnVerde.TabIndex = 2;
            this.btnVerde.UseVisualStyleBackColor = false;
            // 
            // btnRojo
            // 
            this.btnRojo.BackColor = System.Drawing.Color.Red;
            this.btnRojo.Location = new System.Drawing.Point(37, 3);
            this.btnRojo.Name = "btnRojo";
            this.btnRojo.Size = new System.Drawing.Size(27, 23);
            this.btnRojo.TabIndex = 1;
            this.btnRojo.UseVisualStyleBackColor = false;
            // 
            // btnAzul
            // 
            this.btnAzul.BackColor = System.Drawing.Color.Blue;
            this.btnAzul.Location = new System.Drawing.Point(3, 3);
            this.btnAzul.Name = "btnAzul";
            this.btnAzul.Size = new System.Drawing.Size(27, 23);
            this.btnAzul.TabIndex = 0;
            this.btnAzul.UseVisualStyleBackColor = false;
            // 
            // DatosControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelTiempoVida);
            this.Controls.Add(this.labelRecursosAsi);
            this.Controls.Add(this.labelTiempoPedirRecurso);
            this.Controls.Add(this.labelTiempoIni);
            this.Controls.Add(this.labelRecursos);
            this.Controls.Add(this.labNombre);
            this.Name = "DatosControl";
            this.Size = new System.Drawing.Size(528, 100);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labNombre;
        private System.Windows.Forms.Label labelRecursos;
        private System.Windows.Forms.Label labelTiempoIni;
        private System.Windows.Forms.Label labelTiempoPedirRecurso;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label labelRecursosAsi;
        private System.Windows.Forms.Label labelTiempoVida;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnVerde;
        private System.Windows.Forms.Button btnRojo;
        private System.Windows.Forms.Button btnAzul;
        private System.Windows.Forms.Button btnVioleta;
    }
}
