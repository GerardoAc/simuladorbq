﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Collections;
using System.Timers;

namespace SimuladorBq
{
    public partial class Simulador : Form
    {
        private char[] alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        private int enFuncionamiento = 0;
        private Proceso p1;
        private int ultimaLetra = 0;
        private int generarNuevoProceso;
        private int maximoProcesos = 20;
        private int estadoInseguro = 0;
        private ArrayList listaProcesos = new ArrayList();
        private ArrayList listaNombreControlador = new ArrayList();
        private List<Proceso> nuevaL = new List<Proceso>();
        private ArrayList hilos= new ArrayList();
        private string nombreProcesoActual;

        private int cantidadRecursos = 4; // posición 0 = CPU, posición 1 = RAM, posición 2 = HDD, posición 4 = SSD 
        public Simulador()
        {
            //cargar recursos de por defecto


            InitializeComponent();
            
        }
        private void btnInicio_Click(object sender, EventArgs e)
        {

            bool iniciarProcesos = false;
            if (enFuncionamiento == 0)
            {
                RecursoSistema.crearArreglo(cantidadRecursos);

                if (verificarDatos()==4)
                {
                    RecursoSistema.cargarRecurso(0, recursos1.getCPU());
                    RecursoSistema.cargarRecurso(1, recursos1.getRAM());
                    RecursoSistema.cargarRecurso(2, recursos1.getHDD());
                    RecursoSistema.cargarRecurso(3, recursos1.getSDD());
                    iniciarProcesos = true;
                }
                //else
                //{
                //    MessageBox.Show("Como no se ingresaron valores se asignaran unos por defecto");
                //    RecursoSistema.cargarRecurso(0, 300);
                //    RecursoSistema.cargarRecurso(1, 512);
                //    RecursoSistema.cargarRecurso(2, 256);
                //    RecursoSistema.cargarRecurso(3, 256);
                //}

                if (iniciarProcesos== true)
                {
                    myTimer.Interval = 1000;
                    myTimer.Tick += new EventHandler(refrescarInterfaz);
                    myTimer.Start();

                    enFuncionamiento = 1;
                }
            }
            
        }
        public int verificarDatos()
        {
            int contador = 0;
            if (recursos1.getCPU() > 5)
            {
                contador++;
            }
            if (recursos1.getRAM() > 5)
            {
                contador++;
            }
            if (recursos1.getHDD() > 5)
            {
                contador++;
            }
            if (recursos1.getSDD() > 5)
            {
                contador++;
            }
            
            return contador;
        }
        private void generarValoresRand()
        {
            //Random random = new Random();

            //tiempoVida = random.Next(1, 30);
            //tiempoMaxInacicion= random.Next(1, 30);
            //MessageBox.Show(tiempoVida.ToString(),tiempoMaxInacicion.ToString());
;
        }
        private void refrescarInterfaz(object Sender, EventArgs e)
        {

           
            if (listaProcesos.Count != 0)
            {
               // ContenedorDatos.Controls.Clear();
                for(int x=0;x<listaProcesos.Count;x++)
                {
                
                    Proceso p = (Proceso)listaProcesos[x];
                    DatosControl datosC = (DatosControl)ContenedorDatos.Controls[x];
                    if (p.Bloqueo == 1 && p.ImortalProceso == 0)
                    {
                        datosC.BackColor = Color.Green;
                    }
                    else
                    {
                        if (p.Bloqueo != 1 && p.EstadoProceso != 2 && p.ImortalProceso == 0)
                        {
                            datosC.BackColor = Color.Blue;

                        }
                    }
                    if (p.ImortalProceso == 1) {
                        datosC.BackColor = Color.BlueViolet;
                    }
                    String valoresRecursoAsig = p.RecursosAsignados[0].ToString() + "-" + p.RecursosAsignados[1].ToString() +
                       "-" + p.RecursosAsignados[2].ToString() + "-" + p.RecursosAsignados[3].ToString();
                    String valoresRecurso = p.RecursosNecesarios[0].ToString() + "-" + p.RecursosNecesarios[1].ToString() +
                        "-" + p.RecursosNecesarios[2].ToString() + "-" + p.RecursosNecesarios[3].ToString();
                    datosC.Name = (string)listaNombreControlador[x].ToString();
                    datosC.AgregarDatos(p.Nombre,valoresRecurso, p.TiempoInanicion.ToString(), p.TiempoPedirRecursos.ToString(),valoresRecursoAsig,p.TiempoVida.ToString());
                     // ContenedorDatos.Controls.Add(datosC);

                }
            }
            //verificar el bloque o estado inseguro
            if (listaProcesos.Count>1)
            {
                verificarEstadoInseguro();
                //Verificar si se termino algun proceso
                verificarProcesoTermino();
                //Parte para generar un nuevo proceso durante la ejecucion
            }

            seDebegenerarOtroProceso();

            //Parte para actualizar los datos de la tabla

            recursos1.recursosActuales(RecursoSistema.Recursos[0].ToString(), RecursoSistema.Recursos[1].ToString(), RecursoSistema.Recursos[2].ToString(),
            RecursoSistema.Recursos[3].ToString());

        }
        private void seDebegenerarOtroProceso()
        {
            Random random = new Random();
            generarNuevoProceso = random.Next(1, 30);
            if (generarNuevoProceso > 25 && ultimaLetra < maximoProcesos)
            {

                p1 = new Proceso(Path.GetRandomFileName(), cantidadRecursos);
                ultimaLetra++;
                String valoresAsig = p1.RecursosAsignados[0].ToString() + "-" + p1.RecursosAsignados[1].ToString() + "-" + p1.RecursosAsignados[2].ToString()+"-"+p1.RecursosAsignados[3].ToString() ;
                CorrerProceso();
                DatosControl datosC = new DatosControl();
                datosC.Name = "proceso" + p1.Nombre;
                datosC.AgregarDatos(p1.Nombre,imprimir(),p1.TiempoInanicion.ToString(),p1.TiempoPedirRecursos.ToString(),valoresAsig,p1.TiempoVida.ToString());
                
                    
                    ContenedorDatos.Controls.Add(datosC);
                  listaNombreControlador.Add(datosC.Name);
               
                listaProcesos.Add(p1);
            }
            
        }
        private void CorrerProceso()
        {
            p1.inicio();
            
        }
        public string imprimir()
        {
            string cantidadR = "";
            for (int i = 0; i < cantidadRecursos; i++)
            {
                cantidadR += p1.RecursosNecesarios[i] + "-";
            }
            return cantidadR;
        }
        private void verificarProcesoTermino()
        {
            int indice;
            indice = listaProcesos.Count;
            for (int i = 0; i < indice; i++)
            {
                Proceso p = (Proceso)listaProcesos[i];
                if (p.EstadoProceso == 2)
                {
                    DatosControl datosC = (DatosControl)ContenedorDatos.Controls[i];

                    datosC.BackColor = Color.Red;
                    estadoInseguro = 0;
                }
            }
        }
        private void verificarEstadoInseguro()
        {
            int cantidadProcesos;
            cantidadProcesos = listaProcesos.Count;
            int contador;
            contador = 0;
            for (int i = 0; i < cantidadProcesos; i++)
            {
                Proceso p = (Proceso)listaProcesos[i];
                if (p.Bloqueo == 1 && p.EstadoProceso!=2)
                {
                    contador++;
                }
            }
            if (contador >= cantidadProcesos)
            {
                
                for (int i = 0; i < cantidadProcesos; i++)
                {
                    Proceso p = (Proceso)listaProcesos[i];
                    if (p.Bloqueo == 1)
                    {
                        p.EstadoProceso = 0;
                    }
                }
                
                
            }
            
        }
    

        
    }
}
