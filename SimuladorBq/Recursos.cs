﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorBq
{
    public partial class Recursos : UserControl
    {
        public Recursos()
        {
            InitializeComponent();
        }

        private void Recursos_Load(object sender, EventArgs e)
        {

        }
        public void recursosActuales(string cpu,string ram,string HDD,string SDD)
        {
            labelCPUActual.Text ="CPU Actual:"+ cpu;
            labelRamActual.Text ="Ram Acutal:"+ram;
            labelHDDActual.Text = "HDD Actual:"+HDD;
            labelSDDActual.Text = "SDD Actual:"+SDD;

        }
      public int getCPU() {
            errorProvider1.Clear();
            if (txtCPU.Text.Length == 0)
            {
                errorProvider1.SetError(txtCPU, "Ingrese un valor");
                return 0;
            }
            else
            {
                if(0<= Int32.Parse(txtCPU.Text) && Int32.Parse(txtCPU.Text) <= 5)
                {
                    errorProvider1.SetError(txtCPU, "El valor es menor o igual a 5");
                }
                return Int32.Parse(txtCPU.Text);

            }
        }
        public int getRAM()
        {
            errorProvider2.Clear();
            if (txtRAM.Text.Length == 0)
            {
                errorProvider2.SetError(txtRAM, "Ingrese un valor");
                return 0;
            }
            else
            {
                if (0 <= Int32.Parse(txtRAM.Text) && Int32.Parse(txtRAM.Text) <= 5)
                {
                    errorProvider2.SetError(txtRAM, "El valor es menor o igual a 5");
                }
                return Int32.Parse(txtRAM.Text);

            }
        }
        public int getHDD()
        {

            errorProvider3.Clear();
            if (txtHDD.Text.Length == 0)
            {
                errorProvider3.SetError(txtHDD, "Ingrese un valor");
                return 0;
            }
            else
            {
                if (0 <= Int32.Parse(txtHDD.Text) && Int32.Parse(txtHDD.Text) <= 5)
                {
                    errorProvider3.SetError(txtHDD, "El valor es menor o igual a 5");
                }
                return Int32.Parse(txtHDD.Text);

            }
        }
            public int getSDD()
        {
            errorProvider4.Clear();
            if (txtSSD.Text.Length == 0)
            {
                errorProvider4.SetError(txtSSD, "Ingrese un valor");
                return 0;
            }
            else
            {
                if (0 <= Int32.Parse(txtSSD.Text) && Int32.Parse(txtSSD.Text) <= 5)
                {
                    errorProvider4.SetError(txtSSD,"El valor es menor o igual a 5");
                }
                return Int32.Parse(txtSSD.Text);

            }
        }
       

        private void txtCPU_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtRAM_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtHDD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtSSD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
}
