﻿namespace SimuladorBq
{
    partial class Simulador
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInicio = new System.Windows.Forms.Button();
            this.ContenedorDatos = new System.Windows.Forms.FlowLayoutPanel();
            this.recursos1 = new SimuladorBq.Recursos();
            this.SuspendLayout();
            // 
            // btnInicio
            // 
            this.btnInicio.Location = new System.Drawing.Point(117, 72);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Size = new System.Drawing.Size(75, 23);
            this.btnInicio.TabIndex = 1;
            this.btnInicio.Text = "Inicio";
            this.btnInicio.UseVisualStyleBackColor = true;
            this.btnInicio.Click += new System.EventHandler(this.btnInicio_Click);
            // 
            // ContenedorDatos
            // 
            this.ContenedorDatos.AutoScroll = true;
            this.ContenedorDatos.Location = new System.Drawing.Point(213, 143);
            this.ContenedorDatos.Name = "ContenedorDatos";
            this.ContenedorDatos.Size = new System.Drawing.Size(548, 414);
            this.ContenedorDatos.TabIndex = 3;
            // 
            // recursos1
            // 
            this.recursos1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.recursos1.Location = new System.Drawing.Point(213, 12);
            this.recursos1.Name = "recursos1";
            this.recursos1.Size = new System.Drawing.Size(548, 125);
            this.recursos1.TabIndex = 4;
            // 
            // Simulador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(921, 569);
            this.Controls.Add(this.recursos1);
            this.Controls.Add(this.ContenedorDatos);
            this.Controls.Add(this.btnInicio);
            this.HelpButton = true;
            this.MinimizeBox = false;
            this.Name = "Simulador";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simulador";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnInicio;
        private System.Windows.Forms.FlowLayoutPanel ContenedorDatos;
        private Recursos recursos1;
    }
}

