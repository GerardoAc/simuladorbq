﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorBq
{
    class Proceso
    {
        System.Windows.Forms.Timer myTimer1 = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer myTimer2= new System.Windows.Forms.Timer();
        private int imortalProceso = 0;
        private string nombre;
        private int tiempoVida;
        private int tiempoTotalVida;
        private int tiempoInanicion;
        private int tiempoTotalInanicion;
        private int tiempoPedirRecursos;
        private int tiempoPedirTotalRecursos;
        private int estadoProceso; /*el estado del proceso se define como 1 en proceso 0 en espera*/
        private int[] recursosNecesarios; /*estos recursos se acomodan de acuerdo a el arreglo de recursos de el sistema*/
        private int[] recursosAsignados;
        private int canditadRecu;
        private int bloqueo;/* Bloque es 1 0 = sin bloqueo*/
        private int contador = 0;
        private int ejecusionMinima = 0;
  
        public string Nombre { get => nombre; set => nombre = value; }
        public int TiempoVida { get => tiempoVida; set => tiempoVida = value; }
        public int TiempoInanicion { get => tiempoInanicion; set => tiempoInanicion = value; }
        public int TiempoPedirRecursos { get => tiempoPedirRecursos; set => tiempoPedirRecursos = value; }
        public int EstadoProceso { get => estadoProceso; set => estadoProceso = value; }
        public int[] RecursosNecesarios { get => recursosNecesarios; set => recursosNecesarios = value; }
        public int[] RecursosAsignados { get => recursosAsignados; set => recursosAsignados = value; }
        public int CanditadRecu { get => canditadRecu; set => canditadRecu = value; }
        public int Bloqueo { get => bloqueo; set => bloqueo = value; }
        public int ImortalProceso { get => imortalProceso; set => imortalProceso = value; }

        public Proceso(string nombre,int cantidad)
        {
            this.nombre = nombre;
            this.recursosNecesarios = new int[cantidad];
            this.recursosAsignados = new int[cantidad];
            this.canditadRecu = cantidad;
            generarDatos(cantidad);
            this.bloqueo = 0;
            this.estadoProceso = 0;
          
        }
        public void inicio()
        {

            myTimer2.Interval = 1000;
            myTimer2.Tick += new EventHandler(verificarProceso);
            myTimer2.Start();
       
        }


        public void verificarProceso(object Sender, EventArgs e)
        {
         
            if (this.estadoProceso == 0 && this.imortalProceso == 0)
            {
                this.tiempoInanicion--;
                if (this.tiempoInanicion <= 0)
                {
                    this.estadoProceso = 2;//fin del proceso
                    this.bloqueo = 0;
                    liberarRecursos();
                    myTimer2.Stop();
                }
            }
            if (this.estadoProceso == 1)
            {
                this.tiempoVida--;
                if (this.tiempoVida <= 0)
                {
                    this.estadoProceso = 2;//fin del proceso
                    this.bloqueo = 0;
                    liberarRecursos();
                    myTimer2.Stop();
                }
                ejecusionMinima--;
                if (ejecusionMinima==0)
                {
                    cambiarRecursosNecesarios();
                    this.tiempoInanicion = this.tiempoTotalInanicion;
                    ejecusionMinima = randomDatos(2, 6);
                }

                


            }
            if (this.estadoProceso==0)
            {
                 if (this.tiempoPedirTotalRecursos == contador)
                {
                    pedirRecursosSistema();
                    contador = 0;
                    this.tiempoPedirRecursos = this.tiempoPedirTotalRecursos;
                }
                else
                {
                    this.TiempoPedirRecursos--;
                    contador++;
                }
            }
           
        }

        public void liberarRecursos()
        {
            for (int i = 0; i < canditadRecu; i++)
            {
                if (recursosAsignados[i]>0)
                {
                    RecursoSistema.Recursos[i] += recursosAsignados[i];
                    recursosAsignados[i] = 0;
                }
                
            }
        }

       /// <summary>
       /// Pide recursos al sistema basandose en random
       /// </summary>
       public void pedirRecursosSistema()
        {
            int valor;
            int valor2;
            int recurso;
            int solicitudCantidad;
            int cantdadRecursosLiberados;
            Random random = new Random();


                    if (random.Next(20, 100) > 50)
                    {
                        for (int i = 0; i < canditadRecu; i++)
                        {
                            if (recursosNecesarios[i]>0 && recursosAsignados[i]<recursosNecesarios[i])
                            {
                                    recurso = i;//randomDatos(0, canditadRecu-1)
                                    valor = (recursosNecesarios[recurso] - recursosAsignados[recurso]);
                                    
                                    if (random.Next(0,100)>50)
                                    {
                                        solicitudCantidad = valor;
                                    }
                                    else
                                    {
                                        solicitudCantidad = random.Next(0, valor);
                                    }
                                    if (RecursoSistema.Recursos[recurso]!=0)
                                    {

                                        if (RecursoSistema.Recursos[recurso] - solicitudCantidad>=0)
                                        {
                                            recursosAsignados[recurso] += solicitudCantidad;
                                            RecursoSistema.Recursos[recurso] -= solicitudCantidad;
                                        }
                                        else
                                        {
                                            recursosAsignados[recurso] += RecursoSistema.Recursos[recurso];
                                            RecursoSistema.Recursos[recurso] -= RecursoSistema.Recursos[recurso];
                                        }
                                        this.bloqueo = 0;

                                    }
                                    else
                                    {
                                        this.bloqueo = 1;
                                        this.estadoProceso = 0;
                                    } 
                            }
                        }
                      
                     }
                else
                {
                    for (int i = 0; i < canditadRecu; i++)
                    {
                        if (recursosAsignados[i]>0)
                        {
                            valor2 = random.Next(0, canditadRecu - 1);
                            cantdadRecursosLiberados = random.Next(0, recursosAsignados[valor2]); 
                            RecursoSistema.Recursos[valor2] += cantdadRecursosLiberados;
                            recursosAsignados[valor2] -= cantdadRecursosLiberados;
                        }
                    }
                    
                }

            puedeEjecutarse();
            
        }
        public void pedirTodos()
        {

        }
        public void cambiarRecursosNecesarios()
        {
            Random random = new Random();
            int cantidadL = 0;
            int valor2 = 0;
            if (random.Next(0,100)> 70)
            {
                int cualRecurso = 0;
                int cambiarRecursos = 0;
                cambiarRecursos = random.Next(0, canditadRecu - 1);
                for (int i = 0; i < cambiarRecursos; i++)
                {
                    cualRecurso = random.Next(0, canditadRecu - 1);
                    cantidadL = random.Next(0, RecursoSistema.RecursosIniciales[cualRecurso]);
                    recursosNecesarios[cualRecurso] = cantidadL;
                    if (recursosNecesarios[cualRecurso] < recursosAsignados[cualRecurso])
                    {
                        valor2 = recursosAsignados[cualRecurso] - recursosNecesarios[cualRecurso];
                        recursosAsignados[cualRecurso] = recursosNecesarios[cualRecurso];
                        RecursoSistema.Recursos[cualRecurso] += valor2;
                    }
                    
                }
                this.estadoProceso = 0;
            }
           
        }
        
        //verifica si el proceso se puede ejecutar
        public void puedeEjecutarse()
        {
            int recursos = 0;
            for (int i = 0; i < canditadRecu; i++)
            {
                if (this.recursosAsignados[i] == this.recursosNecesarios[i] && verSiNecesita() > 0)
                {
                    recursos++;
                }
            }
            if (recursos>=canditadRecu)
            {
                this.estadoProceso = 1; 
            }
            else
            {
                this.estadoProceso = 0;
            }
        }
        public int verSiNecesita()
        {
            int valor = 0;
            for (int i = 0; i < canditadRecu; i++)
            {
                valor += recursosNecesarios[i];
            }
            return valor;
        }
        public void generarDatos(int cantidad)
        {
            cargarDatosVacios();
            Random random = new Random();
            this.tiempoVida = randomDatos(2, 20);
            this.tiempoTotalInanicion = random.Next(20, 50);
            this.tiempoInanicion = this.tiempoTotalInanicion;
            this.ejecusionMinima = randomDatos(2, 20);

            if (randomDatos(2, 100)>95)
            {
                this.imortalProceso = 1;
            }
            else
            {
                this.imortalProceso = 0;
            }
            
            for (int i = 0; i < cantidad; i++)
            {
                recursosNecesarios[i] = random.Next(0, RecursoSistema.RecursosIniciales[i]);
                recursosAsignados[i] = 0;
            }
            this.tiempoPedirRecursos = random.Next(5, 10);
            this.tiempoPedirTotalRecursos = this.tiempoPedirRecursos;
        }
        public void cargarDatosVacios()
        {
            for (int i = 0; i < canditadRecu; i++)
            {
                recursosNecesarios[i] = 0;
                recursosAsignados[i] = 0;
            }
        }
        public int randomDatos(int mini, int max)
            {
            int v1, v2;
                Random random = new Random();
                v1 = random.Next(mini, max);
                v2 = random.Next(mini, max);
                if (v1>v2)
                {
                    return random.Next(v2, v1);
                }
                return random.Next(v1, v2);
        }

    }
}
